module.exports = {
  chainWebpack(config) {
    config.module.rule('svg')
      .exclude.add(/\.inline\./);

    config.module.rule('vue-svg')
      .test(/\.inline\.svg/)
      .use('vue-loader')
      .loader('vue-loader-v16')
      .end()
      .use('vue-svg-loader')
      .loader('vue-svg-loader');

    config.module.rule('scss')
      .oneOf('vue-modules')
      .use('css-loader')
      .tap((options) => ({
        ...options,
        modules: {
          localIdentName:
            process.env.NODE_ENV === 'development'
              ? options.modules.localIdentName
              : '[hash:base64:5]',
        },
      }));

    return config;
  },
};
