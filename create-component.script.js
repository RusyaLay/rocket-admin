/* eslint-disable */

const {
  existsSync,
  mkdirSync,
  writeFileSync,
} = require('fs');

const capitalizeFirstLetter = (string) => string.charAt(0).toUpperCase() + string.slice(1);

const componentName = process.argv
  .find((arg) => arg.includes('name'))
  .split('=')
  [1];
if (!componentName) {
  console.log('[Create Component Error]: Component name is required!');
  process.exit(0);
}

const dir = process.env.DIR;
if (!dir) {
  console.log('[Create Component Error]: Components dir is required!');
  process.exit(0);
}
const componentsDir = `${__dirname}/${dir}`;
const currentComponentDir = `${componentsDir}/${componentName}`;

if (!existsSync(currentComponentDir)) {
  mkdirSync(currentComponentDir);
}

// Vue template
const vueFileContent =
`<template>
  <div :class="$style.${componentName}">
    ${capitalizeFirstLetter(componentName)}
  </div>
</template>

<script lang="ts">
export default {
  name: '${capitalizeFirstLetter(componentName)}',
  setup() {},
};
</script>

<style lang="scss" module>
@import "./${componentName}";
</style>`;

// Scss template
const scssFileContent =
`@import "~@/assets/utils.scss";

.${componentName} {};
`;

writeFileSync(`${currentComponentDir}/${capitalizeFirstLetter(componentName)}.vue`, vueFileContent, (err) => {
  if (err) console.log(`[Create Component Error]: ${err}`);
});
writeFileSync(`${currentComponentDir}/${componentName}.scss`, scssFileContent, (err) => {
  if (err) console.log(`[Create Component Error]: ${err}`);
});
