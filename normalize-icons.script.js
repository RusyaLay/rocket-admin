const {
  readdir,
  readFileSync,
  writeFileSync,
  unlink,
} = require('fs');

const nameRules = [
  [/ /g, ''],
  [/\[/g, '-'],
  [/\+/g, '-'],
  [/]/g, ''],
  [/\.inline/g, ''],
  [/\.svg/g, ''],
];

const attrsRules = [
  [/width="[^"]*"/g, ''],
  [/height="[^"]*"/g, ''],
  [/fill="[^"]*"/g, ''],
];

const replaceByRules = (str = '', rules) => {
  if (rules.length < 1 || !(rules instanceof Array)) return str;
  return rules.reduce((acc, replace) => acc.replace(...replace), str);
};

const normalize = (
  dirname = __dirname,
  output = dirname,
  renameRules = nameRules,
  removeAttrsRules = attrsRules,
) => {
  readdir(dirname, (err, files) => {
    files.forEach((file) => {
      const filePath = `${dirname}/${file}`;
      const newFileName = replaceByRules(file, renameRules).toLowerCase();
      const normalizedName = `${newFileName}.inline.svg`;

      const buffer = replaceByRules(readFileSync(filePath).toString(), removeAttrsRules);

      writeFileSync(`${output}/${normalizedName}`, buffer, (err) => {
        if (err) console.log(`ERROR: ${err}`);
      });
      if (output === dirname) {
        unlink(filePath, (err) => {
          if (err) throw err;
          console.log('path/file.txt was deleted');
        });
      }
    });
  });
};

normalize(`${__dirname}/src/components/icon/assets/basic`);
