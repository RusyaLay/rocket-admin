export const getRandomString = (length = 7): string => Math.random().toString(36).substr(2, length);

export const formatPhoneNumber = (phoneNumber: string | number) => `+${phoneNumber}`;

export const copyToClipboard = async (somethingToCopy: string) => {
  try {
    await navigator.clipboard.writeText(somethingToCopy as string);
  } catch (e) {
    console.error('Unable to copy', e);
  }
};

export const replaceAt = (
  str: string,
  index: number,
  replacement: string,
) => str.substr(0, index) + replacement + str.substr(index + replacement.length);
