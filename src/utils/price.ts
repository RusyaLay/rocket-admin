import { Currency } from '@/hooks/usePrice';

export const formatPrice = (
  amount: number,
  { rate, iso }: Currency,
  fractionDigits = 0,
): string => new Intl.NumberFormat('de-DE', {
  style: 'currency',
  currencyDisplay: 'symbol',
  currency: iso,
  maximumFractionDigits: fractionDigits,
}).format(amount * rate);
