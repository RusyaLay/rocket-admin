export const formatNumber = (amount: number): string => new Intl.NumberFormat('en-US', {
  style: 'decimal',
  notation: 'compact',
  compactDisplay: 'short',
}).format(amount);

export const formatPercents = (amount: number): string => new Intl.NumberFormat('en-US', {
  style: 'percent',
  minimumFractionDigits: 1,
}).format(amount);
