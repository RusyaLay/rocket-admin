export const compose = (...hocs: any[]) => (something: any | any[]) => (
  hocs.reduce((acc, hoc) => hoc(acc), something)
);

// export const formatPrice = (amount: number, currency: Currency): string => compose(
//   withCurrentCurrency(currency),
//   withFormattedPrice(currency),
// )(amount);
