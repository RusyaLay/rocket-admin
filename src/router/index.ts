import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/sign',
    name: 'sign',
    component: () => import('@/components/authLayout/AuthLayout.vue'),
    redirect: {
      name: 'sign-in',
    },
    children: [
      {
        path: 'in',
        name: 'sign-in',
        component: () => import(/* webpackMode: "lazy" */'@/pages/signIn/SignIn.vue'),
      },
      {
        path: 'lockscreen',
        name: 'lockscreen',
        component: () => import(/* webpackMode: "lazy" */'@/pages/lockscreen/Lockscreen.vue'),
      },
      {
        path: 'up',
        name: 'sign-up',
        component: () => import(/* webpackMode: "lazy" */'@/pages/signUp/SignUp.vue'),
      },
      {
        path: 'forgot',
        name: 'forgot-password',
        component: () => import(/* webpackMode: "lazy" */'@/pages/password/forgot/Forgot.vue'),
      },
      {
        path: 'reset/:resetToken',
        name: 'reset-password',
        component: () => import(/* webpackMode: "lazy" */'@/pages/password/reset/Reset.vue'),
      },
    ],
  },
  {
    path: '',
    component: () => import('@/components/mainLayout/MainLayout.vue'),
    children: [
      {
        path: '',
        name: 'index',
        component: () => import('@/pages/index/index.vue'),
      },
      {
        path: 'e-commerce',
        name: 'e-commerce',
        component: () => import('@/pages/eCommerce/ECommerce.vue'),
      },
      {
        path: 'calendar',
        name: 'calendar',
        component: () => import('@/pages/eCommerce/ECommerce.vue'),
      },
      {
        path: 'mail',
        name: 'mail',
        component: () => import('@/pages/eCommerce/ECommerce.vue'),
      },
      {
        path: 'chat',
        name: 'chat',
        component: () => import('@/pages/eCommerce/ECommerce.vue'),
      },
      {
        path: 'tasks',
        name: 'tasks',
        component: () => import('@/pages/eCommerce/ECommerce.vue'),
      },
      {
        path: 'projects',
        name: 'projects',
        component: () => import('@/pages/eCommerce/ECommerce.vue'),
      },
      {
        path: 'file-manager',
        name: 'file-manager',
        component: () => import('@/pages/eCommerce/ECommerce.vue'),
      },
      {
        path: 'notes',
        name: 'notes',
        component: () => import('@/pages/eCommerce/ECommerce.vue'),
      },
      {
        path: 'contacts',
        name: 'contacts',
        component: () => import('@/pages/eCommerce/ECommerce.vue'),
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
