export type Currency = {
  rate: number;
  iso: string;
}

export type ConversionDirection = 1 | 0 | -1;

export type Conversion = {
  percent: number;
  direction: ConversionDirection;
}
