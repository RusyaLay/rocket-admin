export type SignUpModel = {
  fullName: string,
  email: string,
  password: string,
  confirmPasswordInput: string,
  terms: boolean,
}

export type SignInModel = {
  email: string,
  password: string,
  remember: boolean,
}

export type ForgotPasswordModel = {
  email: string,
}

export type ResetPasswordModel = {
  token: string,
  email: string,
  password: string,
  confirmPasswordInput: string,
}
