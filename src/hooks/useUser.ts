import { computed } from 'vue';
import { useStore } from 'vuex';

export type UserData = {
  name: string;
  surname: string;
};

export const useUser = () => {
  const store = useStore();

  const isAuthorized = computed(() => store.getters['user/isAuthorized']);
  const userData = computed(() => store.getters['user/data']);

  return {
    isAuthorized,
    userData,
  };
};
