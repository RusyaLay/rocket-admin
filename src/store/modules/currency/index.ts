import { Module } from 'vuex';
import { StoreState } from '@/store';

import { Currency } from '@/hooks/usePrice';

export type CurrencyState = {
  currency: Currency;
}

type CurrencyModule = Module<CurrencyState, StoreState>;

const module: CurrencyModule = {
  namespaced: true,
  state() {
    return {
      currency: {
        rate: 1,
        iso: 'USD',
      },
    };
  },
  getters: {
    value: (state) => state.currency,
  },
  mutations: {
    setCurrency(state, currency) {
      state.currency = currency;
    },
  },
};

export default module;
