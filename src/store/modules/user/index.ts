import { Module, Plugin } from 'vuex';
import { StoreState } from '@/store';

import { UserData } from '@/hooks/useUser';
import { SignInModel } from '@/hooks/useAuth';

export type UserState = {
  token: string | null;
  data: UserData | null;
}

type UserModule = Module<UserState, StoreState>;

export const userPlugins: Array<Plugin<StoreState>> = [
  (store) => {
    store.watch(
      (state) => state.user.token,
      async (isAuthorized) => {
        // console.log(router.currentRoute.value);
      }, {
        immediate: true,
      },
    );
  },
];

const module: UserModule = {
  namespaced: true,
  state() {
    return {
      token: null,
      data: null,
    };
  },
  getters: {
    token(state) {
      return state.token;
    },
    data(state) {
      return state.data;
    },
    isAuthorized(state) {
      return !!state.token;
    },
  },
  mutations: {
    setToken(state, token: string) {
      state.token = token;
    },
    setData(state, data: UserData) {
      state.data = data;
    },
  },
  actions: {
    async signIn({ commit }, payload: SignInModel) {
      commit('setToken', btoa(JSON.stringify(payload)));
      commit('setData', {
        id: Math.round(Math.random() * 1000),
        name: 'Ronald',
        surname: 'Robertson',
      });
    },
  },
};

export default module;
