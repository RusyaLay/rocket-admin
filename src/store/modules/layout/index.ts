import { Module } from 'vuex';
import { StoreState } from '@/store';

export type LayoutState = {
  isSidebarExpanded: boolean;
}

type LayoutModule = Module<LayoutState, StoreState>;

const module: LayoutModule = {
  namespaced: true,
  state() {
    return {
      isSidebarExpanded: true,
    };
  },
  getters: {
    isSidebarExpanded(state) {
      return state.isSidebarExpanded;
    },
  },
  mutations: {
    setSidebarExpanded(state, isSidebarExpanded: boolean) {
      state.isSidebarExpanded = isSidebarExpanded;
    },
  },
  actions: {
    toggleSidebar({ commit, getters }) {
      commit('setSidebarExpanded', !getters.isSidebarExpanded);
    },
  },
};

export default module;
