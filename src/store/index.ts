import { createStore } from 'vuex';

import user, { userPlugins, UserState } from '@/store/modules/user';
import layout, { LayoutState } from '@/store/modules/layout';
import currency, { CurrencyState } from '@/store/modules/currency';

export type StoreState = {
  user: UserState;
  layout: LayoutState;
  currency: CurrencyState;
}

export default createStore<StoreState>({
  modules: {
    user,
    layout,
    currency,
  },
  plugins: [
    ...userPlugins,
  ],
});
