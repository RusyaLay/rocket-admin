# rocket-admin

## Project setup
```
yarn
```

### Compiles and hot-reloads for development
```
yarn serve
```

## Create component in src/components
```
yarn create-component --name=<component-name>
```

## Create page in src/pages
```
yarn create-page --name=<component-name>
```


### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
